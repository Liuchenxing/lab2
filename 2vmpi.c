#include<stdlib.h>
#include<mpi.h>
#include<stdio.h>
 

int w1[8] = {-1, 0, 1, -1, 1, -1, 0, 1};
int w2[8] = {-1, -1, -1, 0, 0, 1, 1, 1};
void outputs(int x,int y,int *arr) {
	FILE *fp=fopen("outputs.txt","w");
	for (int i = 1; i <= x; i ++) {
		for (int j = 1; j <= y; j ++){
			printf(" %d ",arr[i*y+j]);
			fprintf(fp," %d ",arr[i*y+j]);
		}
		fputs("\n",fp);
		printf("\n");
	}
	fclose(fp);
	return ;
}
void vonN(int x,int y,int *arr,int size,int myid) {
	

for(int tt=0;tt<300;tt++){
	for (int i = 1; i <= x; i ++) {
		for (int j = 1; j <= y; j ++){
			int temp = 0;
			if(x-myid+i*size>j){
			for (int k = 0; k < 8; k ++) {
					
				if((i + w1[k]<=x && i + w1[k] >=1)||(j + w2[k]<=y && j + w2[k]>=1)){
					if (arr [(i + w1[k])*y+j + w2[k]] == 1) 
						temp ++;
				}
			}
			}
			else{
				for (int k = 0; k < 8; k ++) {
					
				if((i + w1[k]<=x && i + w1[k] >=1)||(j + w2[k]<=y && j + w2[k]>=1)){
					if (arr [(i + w1[k])*y+j + w2[k]] == 1) 
						temp ++;
				}
			}
			}
			if (arr [i*y+j] == 1 && temp == 4 )
				arr [i*y+j] = 1;
			else if (arr [i*y+j] == 0 && (temp == 3 || temp == 2)) 
				arr [i*y+j] = 1;
			else
				arr [i*y+j] = 0;
			
		}
	}
}
} 

int main (int argc, char** argv) {
	int x=0,y=0;
    x=atoi(argv[1]);
	y=atoi(argv[2]);
	
	int *arr=(int*)malloc(sizeof(int)*(x+2)*(y+2));
int *arr1=(int*)malloc(sizeof(int)*(x+2)*(y+2));
	int myid = 0, size = 0;
	MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &myid);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
	if(x%size!=0){
		printf("x should be div by size\n");
		return 0;
	}
	if(y%size!=0){
		printf("y should be div by size\n");
		return 0;
	}	
	for (int i = 1; i <= x/size; i ++) {
		for (int j = 1; j <= y/size; j ++){
				arr[i*y+j]=rand()%2;
		}
	}
		
	MPI_Gather(arr,(x/size)*(y/size),MPI_INT,arr1,(x/size)*(y/size),MPI_INT,0,MPI_COMM_WORLD);
	MPI_Bcast(arr1,x*y,MPI_INT,0,MPI_COMM_WORLD);
	vonN(x,y,arr1,size,myid);
	
	if(myid==0){	
		outputs(x,y,arr1);
	}
	free(arr1);
	free(arr);
	MPI_Finalize();
	return 0;
} 
